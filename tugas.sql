-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2021 at 03:51 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugas`
--

-- --------------------------------------------------------

--
-- Table structure for table `custom_mapping`
--

CREATE TABLE `custom_mapping` (
  `id_dosen` int(11) NOT NULL,
  `nama_belakang` varchar(255) DEFAULT NULL,
  `nama_depan` varchar(255) DEFAULT NULL,
  `nama_mahasiswa` varchar(255) DEFAULT NULL,
  `nama_matkul` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `nim_mahasiswa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `id_dosen` int(5) NOT NULL,
  `nama_depan` varchar(50) NOT NULL,
  `nama_belakang` varchar(50) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `nip` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`id_dosen`, `nama_depan`, `nama_belakang`, `gender`, `nip`) VALUES
(1, 'Budi', 'Rahardja', 'l', '19650109032001'),
(2, 'Basuki', 'Purnama', 'l', '19820503052018'),
(3, 'Joko', 'Widodo', 'l', '19630821042009');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(2);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mhs` int(5) NOT NULL,
  `nama_depan` varchar(50) NOT NULL,
  `nama_belakang` varchar(50) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `nim` varchar(15) NOT NULL,
  `tanggal_lahir` varchar(30) NOT NULL,
  `tanggal_masuk` varchar(30) NOT NULL,
  `dosen_wali` int(5) NOT NULL,
  `dosen_id_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id_mhs`, `nama_depan`, `nama_belakang`, `gender`, `nim`, `tanggal_lahir`, `tanggal_masuk`, `dosen_wali`, `dosen_id_dosen`) VALUES
(1, 'Arina', 'Manasikana', 'p', '123160058', '8 Juli 1998', '28 Agustus 2016', 1, NULL),
(2, 'Ahmad', 'Falahul', 'l', '123160048', '8 Januari 1998', '28 Agustus 2016', 2, NULL),
(3, 'Asrina', 'Maharani', 'p', '123160080', '27 Juni 1998', '28 Agustus 2016', 1, NULL),
(4, 'laelatuz', 'zahro', 'p', '123160070', '12 Agustus 1998', '29 Agustus 2016', 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

CREATE TABLE `matakuliah` (
  `id_matkul` int(5) NOT NULL,
  `kode` varchar(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `pengampu` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `matakuliah`
--

INSERT INTO `matakuliah` (`id_matkul`, `kode`, `nama`, `pengampu`) VALUES
(1, 'IF001', 'Algoritma Pemrograman', 1),
(2, 'IF002', 'Sistem Digital', 2),
(3, 'IF003', 'Riset Operasi', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `custom_mapping`
--
ALTER TABLE `custom_mapping`
  ADD PRIMARY KEY (`id_dosen`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id_dosen`),
  ADD UNIQUE KEY `nip` (`nip`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mhs`),
  ADD UNIQUE KEY `nim` (`nim`),
  ADD KEY `dosen_wali` (`dosen_wali`),
  ADD KEY `FKi5m6wbt0thu4l7s2wia94o9tr` (`dosen_id_dosen`);

--
-- Indexes for table `matakuliah`
--
ALTER TABLE `matakuliah`
  ADD PRIMARY KEY (`id_matkul`),
  ADD UNIQUE KEY `kode` (`kode`),
  ADD KEY `pengampu` (`pengampu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id_dosen` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id_mhs` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `matakuliah`
--
ALTER TABLE `matakuliah`
  MODIFY `id_matkul` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `FKi5m6wbt0thu4l7s2wia94o9tr` FOREIGN KEY (`dosen_id_dosen`) REFERENCES `dosen` (`id_dosen`),
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`dosen_wali`) REFERENCES `dosen` (`id_dosen`);

--
-- Constraints for table `matakuliah`
--
ALTER TABLE `matakuliah`
  ADD CONSTRAINT `matakuliah_ibfk_1` FOREIGN KEY (`pengampu`) REFERENCES `dosen` (`id_dosen`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
