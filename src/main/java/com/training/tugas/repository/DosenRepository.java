package com.training.tugas.repository;

import com.training.tugas.model.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DosenRepository extends JpaRepository<Dosen,Integer> {
    Dosen findByNip(String nip);
}
