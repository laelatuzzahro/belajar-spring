package com.training.tugas.repository;

import com.training.tugas.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MahasiswaRepository extends JpaRepository<Mahasiswa,Integer> {
    Mahasiswa findByNim(String nim);
}
