package com.training.tugas.repository;

import com.training.tugas.model.Matakuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatakuliahRepository extends JpaRepository<Matakuliah,Integer> {
    Matakuliah findByKode(String kode);
}
