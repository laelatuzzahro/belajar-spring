package com.training.tugas.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@SqlResultSetMapping(name = "NativeQuery", entities = {
        @EntityResult( entityClass = CustomMapping.class, fields = {
                @FieldResult(name = "id", column = "id"),
                @FieldResult(name = "namaDepan", column = "nama_depan"),
                @FieldResult(name = "namaBelakang", column = "nama_belakang"),
                @FieldResult(name = "namaMatkul", column = "nama"),
                @FieldResult(name = "nimMahasiswa", column = "nim"),
        })
})
public class CustomMapping {
    @Id
    private String id;
    private String namaDepan;
    private String namaBelakang;
    private String namaMatkul;
    private String nimMahasiswa;
}
