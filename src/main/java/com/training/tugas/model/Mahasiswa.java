package com.training.tugas.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "mahasiswa")
public class Mahasiswa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_mhs")
    private int id;

    @Column(name = "nama_depan")
    private String namaDepan;

    @Column(name = "nama_belakang")
    private String namaBelakang;

    @Column(name = "gender")
    private String gender;

    @Column(name = "nim")
    private String nim;

    @Column(name = "tanggal_lahir")
    private String tanggalLahir;

    @Column(name = "tanggal_masuk")
    private String tanggalMasuk;

    @ManyToOne
    @JoinColumn(name = "dosen_wali")
    private Dosen dosen;
}
