package com.training.tugas.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "dosen")
public class Dosen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_dosen")
    private int id;

    @Column(name = "nama_depan")
    private String namaDepan;

    @Column(name = "nama_belakang")
    private String namaBelakang;

    @Column(name = "gender")
    private String gender;

    @Column(name = "nip")
    private String nip;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "dosen_wali" )
    private List<Mahasiswa> mahasiswaList;

    @OneToOne
    @PrimaryKeyJoinColumn //menandakan bahwa pk pada dosen dipake sebagai fk pada matakuliah
    private Matakuliah matakuliah;


}
