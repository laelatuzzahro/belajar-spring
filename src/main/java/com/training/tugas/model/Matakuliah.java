package com.training.tugas.model;

import com.training.tugas.dto.DtoMatakuliah;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "matakuliah")
@NoArgsConstructor
@AllArgsConstructor
public class Matakuliah {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_matkul")
    private int id;

    @Column(name = "kode")
    private String kode;

    @Column(name = "nama")
    private String nama;

    @OneToOne
    @JoinColumn(name = "pengampu") //nama id yang dijadikan fk
    private Dosen dosen;

}
