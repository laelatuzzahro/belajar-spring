package com.training.tugas.service;

import com.training.tugas.dto.DtoDosen;
import com.training.tugas.dto.DtoMahasiswa;
import com.training.tugas.dto.DtoMatakuliah;
import com.training.tugas.dto.MahasiswaRequest;
import com.training.tugas.model.Dosen;
import com.training.tugas.model.Mahasiswa;
import com.training.tugas.repository.DosenRepository;
import com.training.tugas.repository.MahasiswaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MahasiswaService {

    @Autowired
    MahasiswaRepository mahasiswaRepository;

    @Autowired
    DosenRepository dosenRepository;

    public List<DtoMahasiswa> getAllMahasiswa(){
        List<Mahasiswa> mahasiswas = mahasiswaRepository.findAll();
        List<Dosen> dosens = dosenRepository.findAll();

        ModelMapper mm = new ModelMapper();

        List<DtoDosen> dtoDosens = dosens.stream().map(x -> mm.map(x,DtoDosen.class)).collect(Collectors.toList());
        List<DtoMahasiswa> mahasiswaList = mahasiswas.stream().map(x -> mm.map(x, DtoMahasiswa.class)).collect(Collectors.toList());

        for (int i=0; i<mahasiswas.size(); i++){
            for (int j=0; j<dosens.size(); j++) {
                if (mahasiswas.get(i).getDosen().getId() == dosens.get(j).getId()) {
                    mahasiswaList.get(i).setDosenWali(dtoDosens.get(j).getNamaDepan(),dtoDosens.get(j).getNamaBelakang());
                }
            }
        }

        return mahasiswaList;
    }

    public String saveMahasiswa(MahasiswaRequest mahasiswaRequest){
        Mahasiswa existMahasiswa = mahasiswaRepository.findByNim(mahasiswaRequest.getNim());
        if (existMahasiswa != null){
            return "Data dengan nim tersebut sudah ada";
        }

       Dosen dosen = dosenRepository.getById(mahasiswaRequest.getDosenWali());

        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setNamaDepan(mahasiswaRequest.getNamaDepan());
        mahasiswa.setNamaBelakang(mahasiswaRequest.getNamaBelakang());
        mahasiswa.setGender(mahasiswaRequest.getGender());
        mahasiswa.setNim(mahasiswaRequest.getNim());
        mahasiswa.setTanggalLahir(mahasiswaRequest.getTanggalLahir());
        mahasiswa.setTanggalMasuk(mahasiswaRequest.getTanggalMasuk());
        mahasiswa.setDosen(dosen);
        mahasiswaRepository.save(mahasiswa);
        return "Data berhasil disimpan";
    }

}
