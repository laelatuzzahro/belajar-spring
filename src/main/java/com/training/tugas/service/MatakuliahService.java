package com.training.tugas.service;

import com.training.tugas.dto.DtoDosen;
import com.training.tugas.dto.DtoMatakuliah;
import com.training.tugas.dto.MahasiswaRequest;
import com.training.tugas.dto.MatakuliahRequest;
import com.training.tugas.model.Dosen;
import com.training.tugas.model.Matakuliah;
import com.training.tugas.repository.DosenRepository;
import com.training.tugas.repository.MatakuliahRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MatakuliahService {

    @Autowired
    MatakuliahRepository matakuliahRepository;

    @Autowired
    DosenRepository dosenRepository;

    public List<DtoMatakuliah> getAllMatkul(){
        List<Matakuliah> matakuliahs = matakuliahRepository.findAll();
        List<Dosen> dosens = dosenRepository.findAll();
        ModelMapper mm = new ModelMapper();
        List<DtoMatakuliah> matakuliahList = matakuliahs.stream().map(x->mm.map(x,DtoMatakuliah.class)).collect(Collectors.toList());
        List<DtoDosen> dtoDosens = dosens.stream().map(x -> mm.map(x,DtoDosen.class)).collect(Collectors.toList());

        for (int i = 0; i < matakuliahs.size(); i++) {
            for (int j =0; j < dosens.size(); j++){
                if (matakuliahs.get(i).getDosen().getId() == dosens.get(j).getId()){
                    matakuliahList.get(i).setDosenPengampu(dtoDosens.get(j).getNamaDepan(),dtoDosens.get(j).getNamaBelakang());
                }
            }
        }
        return matakuliahList;

    }

    public String saveMatakuliah(MatakuliahRequest matakuliahRequest){
        Matakuliah existMatakuliah = matakuliahRepository.findByKode(matakuliahRequest.getKode());

        if (existMatakuliah != null){
            return "Matakuliah dengan kode tersebut sudah ada";
        }
        Dosen dosen = dosenRepository.getById(matakuliahRequest.getPengampu());

        Matakuliah matakuliah = new Matakuliah();
        matakuliah.setKode(matakuliahRequest.getKode());
        matakuliah.setNama(matakuliahRequest.getNama());
        matakuliah.setDosen(dosen);
        matakuliahRepository.save(matakuliah);

        return "Matakuliah berhasil ditambahkan";
    }
}
