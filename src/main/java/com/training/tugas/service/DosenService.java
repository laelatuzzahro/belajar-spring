package com.training.tugas.service;

import com.training.tugas.dto.DosenRequest;
import com.training.tugas.dto.DtoDosen;
import com.training.tugas.dto.DtoMahasiswa;
import com.training.tugas.dto.DtoMatakuliah;
import com.training.tugas.model.Dosen;
import com.training.tugas.model.Mahasiswa;
import com.training.tugas.model.Matakuliah;
import com.training.tugas.repository.DosenRepository;
import com.training.tugas.repository.MahasiswaRepository;
import com.training.tugas.repository.MatakuliahRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DosenService {

    @Autowired
    DosenRepository dosenRepository;

    public List<DtoDosen> getAllDosen(){
        List<Dosen> dosens = dosenRepository.findAll();
        ModelMapper mm = new ModelMapper();
        List<DtoDosen> dtoDosens = dosens.stream().map(x -> mm.map(x,DtoDosen.class)).collect(Collectors.toList());

        for (int i=0;i < dosens.size(); i++){
            if (dtoDosens.get(i).getMatakuliah() != null) {
                dtoDosens.get(i).getMatakuliah().setDosenPengampu(dtoDosens.get(i).getNamaDepan(), dtoDosens.get(i).getNamaBelakang());
            }
            for (int j=0; j<dosens.get(i).getMahasiswaList().size(); j++){
                if(dtoDosens.get(i).getMahasiswaList() != null){
                dtoDosens.get(i).getMahasiswaList().get(j).setDosenWali(dtoDosens.get(i).getNamaDepan(),dtoDosens.get(i).getNamaBelakang());}
            }
        }
        return dtoDosens;
    }

    public String saveDosen(DosenRequest dosenRequest){
        Dosen existDosen = dosenRepository.findByNip(dosenRequest.getNip());
        if (existDosen != null){
            return "Dosen dengan NIP tersebut sudah ada";
        }

        Dosen dosen = new Dosen();
        dosen.setNamaDepan(dosenRequest.getNamaDepan());
        dosen.setNamaBelakang(dosenRequest.getNamaBelakang());
        dosen.setGender(dosenRequest.getGender());
        dosen.setNip(dosenRequest.getNip());
        dosenRepository.save(dosen);
        return "Data sudah tersimpan";
    }
}
