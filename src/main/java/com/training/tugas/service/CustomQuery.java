package com.training.tugas.service;

import com.training.tugas.model.CustomMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class CustomQuery {

    @Autowired
    private EntityManager em;

    public List<CustomMapping> getCustomQueryNative(){
        String queryScript = "SELECT RAND(100) AS id, dos.nama_depan, dos.nama_belakang, mat.nama, mhs.nim FROM dosen AS dos INNER JOIN mahasiswa AS mhs ON dos.id_dosen = mhs.dosen_wali INNER JOIN matakuliah AS mat ON dos.id_dosen = mat.pengampu";
        Query q = em.createNativeQuery(queryScript,"NativeQuery");
        List<CustomMapping> list = q.getResultList();
        return list;
    }

}

