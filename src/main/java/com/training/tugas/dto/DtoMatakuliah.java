package com.training.tugas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtoMatakuliah {
    private int id;
    private String kode;
    private String nama;
    private String dosenPengampu;

    public void setDosenPengampu(String namaDepan, String namaBelakang) {
        this.dosenPengampu = namaDepan+" "+namaBelakang;
    }
}
