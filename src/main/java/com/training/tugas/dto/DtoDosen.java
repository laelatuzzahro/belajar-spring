package com.training.tugas.dto;

import com.training.tugas.model.Mahasiswa;
import com.training.tugas.model.Matakuliah;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtoDosen {
    private int id;
    private String namaDepan;
    private String gender;
    private String namaBelakang;
    private String nip;
    private List<DtoMahasiswa> mahasiswaList;
    private DtoMatakuliah matakuliah;

    public DtoDosen(int id, String namaDepan, String namaBelakang, String nip) {
        this.id = id;
        this.namaDepan = namaDepan;
        this.namaBelakang = namaBelakang;
        this.nip = nip;
        this.mahasiswaList = new ArrayList<DtoMahasiswa>();
        this.matakuliah = matakuliah;
    }

    public void addMahasiswa(DtoMahasiswa mahasiswa){
        this.mahasiswaList.add(mahasiswa);
    }
}
