package com.training.tugas.dto;

import com.training.tugas.model.Dosen;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DtoMahasiswa {
    private int id;
    private String namaDepan;
    private String namaBelakang;
    private String gender;
    private String nim;
    private String tanggalLahir;
    private String tanggalMasuk;
    private String dosenWali;

    public void setDosenWali(String namaDepan, String namaBelakang) {
        this.dosenWali = namaDepan+" "+namaBelakang;
    }
}
