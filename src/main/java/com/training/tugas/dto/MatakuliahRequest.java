package com.training.tugas.dto;

import lombok.Data;

@Data
public class MatakuliahRequest {
    private String kode;
    private String nama;
    private int pengampu;
}
