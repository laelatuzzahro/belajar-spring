package com.training.tugas.dto;

import lombok.Data;

@Data
public class MahasiswaRequest {
    private String namaDepan;
    private String namaBelakang;
    private String gender;
    private String nim;
    private String tanggalLahir;
    private String tanggalMasuk;
    private int dosenWali;
}
