package com.training.tugas.dto;

import lombok.Data;

@Data
public class DosenRequest {
    private String namaDepan;
    private String namaBelakang;
    private String gender;
    private String nip;

}
