package com.training.tugas.controller;

import com.training.tugas.dto.*;
import com.training.tugas.model.CustomMapping;
import com.training.tugas.service.CustomQuery;
import com.training.tugas.service.DosenService;
import com.training.tugas.service.MahasiswaService;
import com.training.tugas.service.MatakuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AllController {

    @Autowired
    DosenService dosenService;

    @Autowired
    MahasiswaService mahasiswaService;

    @Autowired
    MatakuliahService matakuliahService;

    @Autowired
    CustomQuery cc;

    @GetMapping("/getalldosen")
    public List<DtoDosen> getAllDosen(){
        return dosenService.getAllDosen();
    }

    @GetMapping("/getallmahasiswa")
    public List<DtoMahasiswa> getAllMahasiswa(){
        return mahasiswaService.getAllMahasiswa();
    }

    @GetMapping("/getallmatakuliah")
    public List<DtoMatakuliah> getAllMatakuliah(){
        return matakuliahService.getAllMatkul();
    }

    @PostMapping(value = "/savedosen", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveDosen(@RequestBody DosenRequest dosenRequest){
        return dosenService.saveDosen(dosenRequest);
    }

    @PostMapping(value = "/savemahasiswa", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveMahasiswa(@RequestBody MahasiswaRequest mahasiswaRequest){
        return mahasiswaService.saveMahasiswa(mahasiswaRequest);
    }

    @PostMapping(value = "/savematakuliah", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveMatakuliah(@RequestBody MatakuliahRequest matakuliahRequest) {
        return matakuliahService.saveMatakuliah(matakuliahRequest);
    }

    @GetMapping("/custommapping")
    public List<CustomMapping> getCustomMapping(){
        return cc.getCustomQueryNative();
    }

}
